<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>该用户所有订单</h1>
			<table border="1">
				<tr>
					<th>订单编号</th>
					<th>订单日期</th>
					<th>订单价格</th>
					<th>客户名称</th>
				</tr>
				<s:iterator value="list">
				<tr>
					<td><s:property value="id"/></td>
					<td><s:property value="orderDate"/></td>
					<td><s:property value="price"/></td>
					<td><s:property value="customer.name"/></td>
				</tr>
				</s:iterator>
			</table>
	</center>
</body>
</html>