<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/"></script> --%>
</head>
<body>
	<center>
		<table border="1">
			<tr>
				<td colspan="6"><a href="${pageContext.request.contextPath}/addCustomerUI">新增客户</a></td>
			</tr>
			<tr>
				<th>编号</th>
				<th>客户名称</th>
				<th>客户性别</th>
				<th colspan="3">操作</th>
			</tr>
			
			<c:forEach items="${pb.list}" var="c">
			<tr>
				<td>${ c.id }</td>
				<td>${ c.name }</td>
				<td><c:if test="${ c.sex==1 }">男</c:if>
				<c:if test="${ c.sex==2 }">女</c:if></td>
				<td><a href="${pageContext.request.contextPath}/findOrderById?id=${ c.id }">查看订单</a></td>
				<td><a href="${pageContext.request.contextPath}/addOrder">修改</a></td>
				<td><a href="#">删除</a></td>
			</tr>
			</c:forEach>
		</table>	
			
			<a href="${pageContext.request.contextPath}/findAll?currPage=1">首页</a>&nbsp;&nbsp;
			共${ pb.totalPage }页/每页${ pb.pageSize }条&nbsp;&nbsp;
			<c:if test="${ pb.currPage == 1 }">
				<a href="javascript:;">上一页</a>&nbsp;&nbsp;
			</c:if>
			<c:if test="${ pb.currPage != 1 }">
			<a href="${pageContext.request.contextPath}/findAll?currPage=${pb.currPage-1}">上一页</a>&nbsp;&nbsp;
			</c:if>
			<c:forEach begin="1" end="${ pb.totalPage }" var="i">
				<c:if test="${ pb.currPage == i }">
					${ i }
				</c:if>
				<c:if test="${ pb.currPage != i }">
					<a href="${pageContext.request.contextPath}/findAll?currPage=${ i }">${ i }</a>
				</c:if>
			</c:forEach>
			<c:if test="${ pb.currPage != pb.totalPage }">
				<a href="${pageContext.request.contextPath}/findAll?currPage=${pb.currPage+1}">下一页</a>&nbsp;&nbsp;
			</c:if>
			<c:if test="${ pb.currPage == pb.totalPage }">
				<a href="javascript:;">下一页</a>
			</c:if>
	</center>
</body>
</html>