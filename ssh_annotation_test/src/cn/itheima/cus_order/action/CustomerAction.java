package cn.itheima.cus_order.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itheima.cus_order.service.CustomerService;
import cn.itheima.domain.Customer;
import cn.itheima.domain.PageBean;

@Controller("customerAction")
@Scope("protype")
@Namespace("/")
@ParentPackage("struts-default")
public class CustomerAction extends ActionSupport implements ModelDriven<Customer>{
	
	private Customer c = new Customer();

	@Override
	public Customer getModel() {
		return c;
	}
	
	@Autowired
	@Qualifier("customerService")
	private CustomerService cs;
	
	@Action(value="findAll",results={@Result(name="success",location="/customer_list.jsp")})
	public String findAll() {
		Integer currPage = Integer.parseInt(ServletActionContext.getRequest().getParameter("currPage"));
		// ����ҵ���
		PageBean pb = cs.findAll(currPage);
		
		ActionContext.getContext().getValueStack().set("pb", pb);
		System.out.println("123");
		System.out.println("789");
		return "success";
	}
	
	@Action(value="addCustomerUI",results={@Result(name="success",location="/addCustomer.jsp")})
	public String addCustomerUI() {
		return SUCCESS;
	}
	
	@Action(value="addCustomer",results={@Result(name="success",type="redirectAction",location="${pageContext.request.contextPath}/findAll?currPage=1")})
	public String addCustomer(){
		cs.addCustomer(c);
		return SUCCESS;
	}
}
