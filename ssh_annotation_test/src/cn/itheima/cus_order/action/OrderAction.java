package cn.itheima.cus_order.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cn.itheima.cus_order.service.OrderService;
import cn.itheima.domain.Order;

@Controller("orderAction")
@Scope("protype")
@Namespace("/")
@ParentPackage("struts-default")
public class OrderAction extends ActionSupport{
	
	private Integer id;
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Autowired
	@Qualifier("orderService")
	private OrderService os;

	@Action(value="findOrderById",results={@Result(name="success",location="/cus_order.jsp")})
	public String findOrderById() {
		List<Order> list = os.findOrderById(id);
		
		ActionContext.getContext().getValueStack().set("list", list);
		return SUCCESS;
	}
	
	@Action(value="addOrder")
	public String addOrder() {
		os.addOrder();
		return null;
	}
}
