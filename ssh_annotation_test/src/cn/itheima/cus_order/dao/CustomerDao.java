package cn.itheima.cus_order.dao;

import java.util.List;

import cn.itheima.domain.Customer;

public interface CustomerDao {

	public List<Customer> findCustomer(Integer begin,Integer pageSize);

	public void addCustomer(Customer c);

	public Integer findTotalCount();

}
