package cn.itheima.cus_order.dao;

import java.util.List;

import cn.itheima.domain.Order;

public interface OrderDao {

	public List<Order> findOrderById(Integer id);
	public void addOrder();
}
