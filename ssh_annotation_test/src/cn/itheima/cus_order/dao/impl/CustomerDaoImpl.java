package cn.itheima.cus_order.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.internal.CriteriaImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import cn.itheima.cus_order.dao.CustomerDao;
import cn.itheima.domain.Customer;

@Repository("customerDao")
public class CustomerDaoImpl extends HibernateDaoSupport implements CustomerDao{
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSuperSessionFactory(SessionFactory sf) {
		setSessionFactory(sf);
	}

	@Override
	public List<Customer> findCustomer(Integer begin,Integer pageSize) {
		//List<Customer> list = (List<Customer>) getHibernateTemplate().find("from Customer");
		/*List<Customer> list = (List<Customer>) getHibernateTemplate().execute(
			new HibernateCallback() {

				@Override
				public List<Customer> doInHibernate(Session session) throws HibernateException {
					Query query = session.createQuery("from Customer");
					query.setFirstResult(begin);
					query.setMaxResults(pageSize);
					List list2 = query.list();
					return list2;
				}
			}
				
				);*/
		DetachedCriteria dc = DetachedCriteria.forClass(Customer.class);
		
		List<Customer> list = (List<Customer>) getHibernateTemplate().findByCriteria(dc, begin, pageSize);
		return list;
	}

	@Override
	public void addCustomer(Customer c) {
		getHibernateTemplate().save(c);
	}

	@Override
	public Integer findTotalCount() {
		Long totalCount = (Long)getHibernateTemplate().find("select count(*) from Customer").listIterator().next();
        return totalCount.intValue();
	}
	
}
