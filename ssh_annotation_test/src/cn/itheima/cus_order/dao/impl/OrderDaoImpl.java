package cn.itheima.cus_order.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import cn.itheima.cus_order.dao.OrderDao;
import cn.itheima.domain.Customer;
import cn.itheima.domain.Order;

@Repository("orderDao")
public class OrderDaoImpl extends HibernateDaoSupport implements OrderDao {
	
	@Autowired
	@Qualifier("sessionFactory")
	public void serSupperSessionFactory(SessionFactory sf) {
		setSessionFactory(sf);
	}

	@Override
	public List<Order> findOrderById(Integer id) {
		List<Order> list = (List<Order>) this.getHibernateTemplate().find("from Order where t_customer_id = ?", id);
		return list;
	}

	@Override
	public void addOrder() {
		Customer customer = this.getHibernateTemplate().get(Customer.class, 1);
		Order o = new Order();
		o.setOrderDate(new Date());
		o.setPrice(1255);
		o.setCustomer(customer);
		this.getHibernateTemplate().save(o);
		
	}

}
