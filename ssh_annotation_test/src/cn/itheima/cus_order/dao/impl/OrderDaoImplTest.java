package cn.itheima.cus_order.dao.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itheima.cus_order.dao.OrderDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public class OrderDaoImplTest {
	
	@Autowired
	@Qualifier("orderDao")
	private OrderDao od;

	@Test
	public void testAddOrder() {
		od.addOrder();
	}

}
