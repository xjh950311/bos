package cn.itheima.cus_order.service;

import java.util.List;

import cn.itheima.domain.Customer;
import cn.itheima.domain.PageBean;

public interface CustomerService {

	public PageBean findAll(Integer currPage);

	public void addCustomer(Customer c);

}
