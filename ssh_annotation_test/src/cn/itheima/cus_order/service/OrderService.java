package cn.itheima.cus_order.service;

import java.util.List;

import cn.itheima.domain.Order;

public interface OrderService {

	public List<Order> findOrderById(Integer id);

	public void addOrder();

}
