package cn.itheima.cus_order.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itheima.cus_order.dao.CustomerDao;
import cn.itheima.cus_order.service.CustomerService;
import cn.itheima.domain.Customer;
import cn.itheima.domain.PageBean;

@Service("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	@Qualifier("customerDao")
	private CustomerDao cd;

	@Override
	public PageBean findAll(Integer currPage) {
		PageBean pb = new PageBean();
		// 设置当前页
		pb.setCurrPage(currPage);
		// 设置每页记录条数
		Integer pageSize = 5;
		pb.setPageSize(pageSize);
		// 设置总记录数
		Integer totalCount = cd.findTotalCount();
		pb.setTotalCount(totalCount);
		// 设置总页数
		Double totalPage = Math.ceil((double)totalCount/pageSize); 
		pb.setTotalPage(totalPage.intValue());
		// 设置数据
		Integer begin = (currPage-1)*pageSize;
		List<Customer> list = cd.findCustomer(begin,pageSize);
		pb.setList(list);
		return pb;
	}

	@Override
	public void addCustomer(Customer c) {
			
			cd.addCustomer(c);
		
	}

}
