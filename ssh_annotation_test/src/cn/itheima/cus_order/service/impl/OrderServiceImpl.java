package cn.itheima.cus_order.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.itheima.cus_order.dao.OrderDao;
import cn.itheima.cus_order.service.OrderService;
import cn.itheima.domain.Order;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	@Qualifier("orderDao")
	private OrderDao od;

	@Override
	public List<Order> findOrderById(Integer id) {
		return od.findOrderById(id);
	}

	@Override
	public void addOrder() {
		od.addOrder();
		
	}

}
